package com.healthtime.project.healthtime.Adapter;

import android.content.Context;
import android.media.Image;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.healthtime.project.healthtime.R;

/**
 * Created by GTC on 2/18/2017.
 */

public class SwipeImageSliderAdapter extends PagerAdapter {

    private int[] image_resource = {R.drawable.slider0,R.drawable.slider1};
    private Context context;
    private LayoutInflater layoutInflater;

    public SwipeImageSliderAdapter(Context context){
        this.context = context;

    }

    @Override
    public int getCount() {
        return image_resource.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == (LinearLayout)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = layoutInflater.inflate(R.layout.swipe_img_layout, container, false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.image_slider);
        imageView.setImageResource(image_resource[position]);
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout)object);
    }
}
