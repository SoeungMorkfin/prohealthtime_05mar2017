package com.healthtime.project.healthtime.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.healthtime.project.healthtime.main.Fragment.ArticleFragment;
import com.healthtime.project.healthtime.main.Fragment.ProfileFragment;
import com.healthtime.project.healthtime.main.Fragment.SubPageFragment;

/**
 * Created by Morkfin on 2/1/2017.
 */

public class TabPagerAdapter extends FragmentPagerAdapter {
    private int tabCount;
    public TabPagerAdapter(FragmentManager fm,int numberOfTab) {
        super(fm);
        this.tabCount = numberOfTab;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                SubPageFragment subPage = new SubPageFragment();
                return subPage;
            case 1:
                ArticleFragment article = new ArticleFragment();
                return article;
            case 2:
                ProfileFragment profileFragment = new ProfileFragment();
                return profileFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
