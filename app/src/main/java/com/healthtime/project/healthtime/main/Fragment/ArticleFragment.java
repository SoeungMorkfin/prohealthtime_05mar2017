package com.healthtime.project.healthtime.main.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.healthtime.project.healthtime.Adapter.SubpageAdapter;
import com.healthtime.project.healthtime.R;

/**
 * Created by Morkfin on 2/1/2017.
 */

public class ArticleFragment extends Fragment{
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.subpage,container,false);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.subpage_recycler_view);
        recyclerView.setHasFixedSize(true);
//        SubpageAdapter subpageAdapter = new SubpageAdapter(new String[]{"Content 4","Content 3","Content 2","Content 1"});
//        recyclerView.setAdapter(subpageAdapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        return rootView;
    }
}
