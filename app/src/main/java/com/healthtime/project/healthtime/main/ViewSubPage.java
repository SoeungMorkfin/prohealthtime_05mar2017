package com.healthtime.project.healthtime.main;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.healthtime.project.healthtime.Adapter.SwipeImageSliderAdapter;
import com.healthtime.project.healthtime.R;

/**
 * Created by GTC on 2/18/2017.
 */

public class ViewSubPage extends AppCompatActivity implements View.OnClickListener{

    private ViewPager viewPager;
    private SwipeImageSliderAdapter adapter;
    private TextView txt_title,txt_address,txt_contact,txt_service_title,txt_read_service_detail,txt_team_name,txt_team_position,txt_read_position_detail;
    private Button btn_booking;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_subpage);

        // configure slider
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        adapter = new SwipeImageSliderAdapter(this);
        viewPager.setAdapter(adapter);

        // configure text view and button
        txt_title = (TextView) findViewById(R.id.txt_title);
        txt_title.setText("មន្ទីរពេទ្យភ្នែក ដុកទ័រ គង់ពិសិដ្ឋ");

        txt_address = (TextView) findViewById(R.id.txt_address);
        txt_address.setText("#108, St.110, ទឹកល្អក់ទី ១ , ទួលគោក, រាជធានីភ្នំពេញ, ខ្មែរ");

        txt_contact = (TextView) findViewById(R.id.txt_contact);
        txt_contact.setText("023 966 601");

        btn_booking = (Button) findViewById(R.id.btn_booking);

        txt_service_title = (TextView) findViewById(R.id.txt_service_title);
        txt_service_title.setText("ភ្នែក");

        txt_read_service_detail = (TextView) findViewById(R.id.txt_read_service_detail);
        txt_read_service_detail.setText("អានបន្ថែម");

        txt_team_name = (TextView) findViewById(R.id.txt_team_name);
        txt_team_name.setText("វេជ្ជបណ្ឌិត គង់ ពិសិដ្ឋ");

        txt_team_position = (TextView) findViewById(R.id.txt_team_position);
        txt_team_position.setText("-ជានាយកនៃមន្ទីរពេទ្យភ្នែ.....");

        txt_read_position_detail = (TextView) findViewById(R.id.txt_read_position_detail);
        txt_read_position_detail.setText("អានបន្ថែម");
    }

    @Override
    public void onClick(View v) {
        if(v==btn_booking){

        }
    }
}
