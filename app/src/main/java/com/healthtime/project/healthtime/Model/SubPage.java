package com.healthtime.project.healthtime.Model;

/**
 * Created by GTC on 2/18/2017.
 */

public class SubPage {
    private String title,address;

    public SubPage() {

    }
    public SubPage(String title,String address){
        this.title = title;
        this.address = address;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
