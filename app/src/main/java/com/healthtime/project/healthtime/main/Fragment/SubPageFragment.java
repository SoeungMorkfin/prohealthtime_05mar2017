package com.healthtime.project.healthtime.main.Fragment;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.healthtime.project.healthtime.Adapter.SubpageAdapter;
import com.healthtime.project.healthtime.Model.SubPage;
import com.healthtime.project.healthtime.R;

import java.util.ArrayList;

/**
 * Created by Morkfin on 2/1/2017.
 */

public class SubPageFragment extends Fragment {

    private ArrayList<SubPage> listContentSubPage = new ArrayList<>();
    private SubpageAdapter subpageAdapter;
    @RequiresApi(api = Build.VERSION_CODES.ECLAIR_MR1)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.subpage,container,false);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.subpage_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(subpageAdapter);

        SubPage subPage = new SubPage();
        subPage.setTitle("មន្ទីរពេទ្យភ្នែក ដុកទ័រ គង់ពិសិដ្ឋ");
        subPage.setAddress("#108, St.110, ទឹកល្អក់ទី ១ , ទួលគោក, រាជធានីភ្នំពេញ, ខ្មែរ");

        listContentSubPage.add(subPage);
        subpageAdapter = new SubpageAdapter(listContentSubPage);
        recyclerView.setAdapter(subpageAdapter);
        subpageAdapter.notifyDataSetChanged();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        return rootView;
    }
}
