package com.healthtime.project.healthtime.main.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.healthtime.project.healthtime.R;

/**
 * Created by GTC on 2/18/2017.
 */

public class ProfileFragment extends Fragment{
//    TextView txtStatus;
//    LoginButton login_button;
//    CallbackManager callbackManager;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getContext());
        View rootView = inflater.inflate(R.layout.profile,container,false);

//        initializeControls();
//        loginWithFB();
        return rootView;
    }
//    private void initializeControls(){
//        callbackManager = CallbackManager.Factory.create();
//        txtStatus = (TextView)getActivity().findViewById(R.id.txtStatus);
//        login_button = (LoginButton)getActivity().findViewById(R.id.login_button);
//
//    }

//    private void loginWithFB(){
//        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                txtStatus.setText("Login Sucess\n"+loginResult.getAccessToken());
//            }
//
//            @Override
//            public void onCancel() {
//                txtStatus.setText("Login Cancel");
//
//            }
//
//            @Override
//            public void onError(FacebookException error) {
//                txtStatus.setText("Login Error" + error.getMessage());
//            }
//        });
//    }
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        callbackManager.onActivityResult(requestCode, resultCode, data);
//    }
}
