package com.healthtime.project.healthtime.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.healthtime.project.healthtime.Model.SubPage;
import com.healthtime.project.healthtime.R;
import com.healthtime.project.healthtime.main.ViewSubPage;

import java.util.ArrayList;

/**
 * Created by Morkfin on 2/5/2017.
 */

public class SubpageAdapter extends RecyclerView.Adapter<SubpageAdapter.MyViewHolder> {
    private ArrayList<SubPage> listContentSubpage;
    private Context context;

    @Override
    public SubpageAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_subpage, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyViewHolder myViewHolder = new MyViewHolder(context, v, listContentSubpage);
        return myViewHolder;
    }

    /*
    * Binding or Setting elements on each contents
    * */
    @Override
    public void onBindViewHolder(SubpageAdapter.MyViewHolder holder, int position) {
        SubPage subpage = listContentSubpage.get(position);

        holder.txt_content_title.setText(subpage.getTitle());
        holder.txt_content_address.setText(subpage.getAddress());


    }

    @Override
    public int getItemCount() {
        return listContentSubpage.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public CardView card_view_sub_id;
        public TextView txt_content_title, txt_content_address;
        public Context context;
        public ArrayList<SubPage> subPageArrayList;

        public MyViewHolder(Context context, View itemView, ArrayList<SubPage> subPageArrayList) {
            super(itemView);
            this.context = context;
            this.subPageArrayList = subPageArrayList;
            card_view_sub_id = (CardView) itemView.findViewById(R.id.card_view_sub_id);
            txt_content_title = (TextView) itemView.findViewById(R.id.txt_content_title);
            txt_content_address = (TextView) itemView.findViewById(R.id.txt_content_address);
            // set on click each contents
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Context context = v.getContext();
            //create a position for calling specific content
            int position = getAdapterPosition();
            SubPage subPage = this.subPageArrayList.get(position);
            Intent intent = new Intent(context, ViewSubPage.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);


        }
    }

    public SubpageAdapter(ArrayList<SubPage> listContentSubpage) {
        this.listContentSubpage = listContentSubpage;
    }

}
